@foreach($products as $key=>$product)
    <tr>
        <td>{{ $product['sku'] }}</td>
        <td>
                                    <span class="d-block font-size-sm text-body">
                                        <a href="{{ route('product.show',['id'=>$product['id']])}}">{{ $product['name'] }}</a>
                                    </span>
        </td>
        <td>
            <a href="{{ route('product.show',['id' => $product['id']]) }}">
                <div class="d-block font-size-sm text-body">
                    <img src="{{ asset('storage/admin/product/'.$product['image']) }}" style="width: 100px" onerror="this.src='{{asset('admin/assets/img/160x160/img2.jpg')}}'">
                </div>
            </a>
        </td>
        <td>
            @if($product['status']==1)
                <div style="padding: 10px;border: 1px solid;cursor: pointer"
                     onclick="location.href='{{route('product.status',['id'=> $product['id']])}}'">
                    <span class="legend-indicator bg-success"></span>Hoạt động
                </div>
            @else
                <div style="padding: 10px;border: 1px solid;cursor: pointer"
                     onclick="location.href='{{ route('product.status',['id'=>$product['id']]) }}'">
                    <span class="legend-indicator bg-danger"></span>Không hoạt động
                </div>
            @endif
        </td>
        {{--                                <td>--}}
        {{--                                    <label class="toggle-switch d-flex align-items-center mb-3" for="pending_status">--}}
        {{--                                        <input type="checkbox" name="pending_status" class="toggle-switch-input" onchange="daily_needs()" value="0" id="pending_status" >--}}
        {{--                                        <span class="toggle-switch-label">--}}
        {{--                                                    <span class="toggle-switch-indicator"></span>--}}
        {{--                                                </span>--}}
        {{--                                    </label>--}}
        {{--                                </td>--}}
        <td>{{ $product['selling_price'] }}</td>
        <td>
            <label class="badge badge-soft-info">{{ $product['total_stock'] }}</label>
        </td>
        <td>
            <!-- Dropdown -->
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="tio-settings"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{ route('product.show', ['id' => $product['id']]) }}">Chi tiết</a>
                    <a class="dropdown-item" href="{{ route('product.edit', ['id' => $product['id']]) }}">Cập nhật</a>
                    <a class="dropdown-item" href="javascript:" onclick="form_alert('{{ $product['id'] }}','Bạn chắc chắn muốn xóa Sản phẩm')">Xóa</a>
                    <form action="{{route('product.delete', ['id' => $product['id']])}}" method="POST" id="{{ $product['id'] }}">
                        @csrf @method('DELETE')
                    </form>
                </div>
            </div>
            <!-- End Dropdown -->
        </td>
    </tr>
@endforeach
