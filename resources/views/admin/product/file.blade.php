@extends('layouts.admin.app')

@section('title', 'Import Product')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-6">
                    <h1 class="page-header-title">Import Product</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->

        <!-- Card -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <!-- Card -->
                <div class="card">
                    <!-- Header -->
                    <div class="card-header" style="padding-right: 0;">
                        <div class="container row">
                            <div class="col-6"></div>
                            <div class="col-md-6">
                                <a href="" class="btn btn-primary float-right"><i class="tio-add-circle"></i>
                                    Dowload Sample file
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2 pt-3">
                        <form action="" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="">
                            <div class="row">
                                <div class="col-8 from_part_2 left">
                                    <label>Import Your File</label><small style="color: red"> (* Only Excel Product)</small>
                                    <div class="custom-file">
                                        <label class="custom-file-label" for="customFileEg1">Choose File</label>
                                        <input type="file" name="file" id="customFileEg1" class="custom-file-input" accept=".xlsx,.csv,.xls" required>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    <!-- End Table -->
                </div>
                <!-- End Card -->
            </div>
        </div>
        <!-- End Card -->
    </div>
@endsection
