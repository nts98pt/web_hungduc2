@extends('layouts.admin.app')

@if(isset($product))
    @section('title', 'Cập nhật sản phẩm')
@else
    @section('title', 'Thêm mới sản phẩm')
@endif

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    @if(isset($product))
                        <h1 class="page-header-title"><i class="tio-edit"></i> Cập nhật sản phẩm</h1>
                    @else
                        <h1 class="page-header-title"><i class="tio-add-circle-outlined"></i> Thêm mới sản phẩm</h1>
                    @endif
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="@if(isset($product)) {{ route('product.update',['id'=>$product->id]) }} @else {{ route('product.store') }} @endif" method="post" id="product_form" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlSelect1">Danh mục<small style="color: red">*</small></label>
                                <select name="category_id" class="form-control js-select2-custom"
                                        onchange="getRequest('{{route('product.get-categories')}}?id='+this.value,'sub-categories')">
                                    <option value="">---Select---</option>
                                    @foreach($categories as $category)
                                        <option onclick="getRequest('{{route('product.get-categories')}}?id='+this.value,'sub-categories')" @if(isset($product) && $category_id->category->id==$category->id) selected="selected" @endif value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlSelect1">Danh mục con<small style="color: red">*</small></label>
                                <select name="sub_category_id" id="sub-categories" class="form-control js-select2-custom">
                                    @if(isset($product))
                                        @foreach($sub_categories as $sub_category)
                                            <option @if($category_id->subcategory->id==$sub_category->id) selected="selected" @endif value="{{ $sub_category->id }}">{{ $sub_category->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="from_part_2">
                        <div class="row">
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Mã sản phẩm<small style="color: red">*</small></label>
                                    <input type="text" name="sku" @if(isset($product)) value="{{ $product->sku }}" @endif class="form-control" id="total" placeholder="Ví dụ : EA100" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Tên sản phẩm<small style="color: red">*</small></label>
                                    <input type="text" name="name" @if(isset($product)) value="{{ $product->name }}" @endif class="form-control" id="total" placeholder="Tên sản phẩm" required>
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Mô tả sản phẩm<small style="color: red">*</small></label>
                                    <textarea name="description" style="height: 200px;" class="form-control" placeholder="Mô tả sản phẩm" >@if(isset($product)) {{ $product->description }} @endif</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Giá bán<small style="color: red">*</small></label>
                                    <input type="number" name="selling_price" @if(isset($product)) value="{{ $product->selling_price }}" @endif class="form-control" placeholder="Giá bán" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Số lượng trong kho<small style="color: red">*</small></label>
                                    <input type="number" name="total_stock" @if(isset($product)) value="{{ $product->total_stock }}" @endif class="form-control" placeholder="Số lượng trong kho" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Giảm giá</label>
                                    <input type="number" min="0" max="100000" @if(isset($product)) value="{{ $product->discount }}" @endif value="0" name="discount" class="form-control" placeholder="Ex : 100">
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Loại giảm giá</label>
                                    <select name="discount_type" class="form-control js-select2-custom">
                                        <option value="">--Loại giảm giá--</option>
                                        <option @if(isset($product) && $product->discount_type=='percent') selected="selected" @endif value="percent">Phần trăm</option>
                                        <option @if(isset($product) && $product->discount_type=='amount') selected="selected" @endif value="amount">Số tiền</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Thuế</label>
                                    <input type="number" min="0" value="0" @if(isset($product)) value="{{ $product->tax }}" @endif step="0.01" max="100000" name="tax" class="form-control" placeholder="Ex : 7" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Loại thuế</label>
                                    <select name="tax_type" class="form-control js-select2-custom">
                                        <option value="">--Loại thuế--</option>
                                        <option @if(isset($product) && $product->tax_type=='amount') selected="selected" @endif value="percent">Phần trăm</option>
                                        <option @if(isset($product) && $product->tax_type=='amount') selected="selected" @endif value="amount">Số tiền</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Nhãn hàng</label>
                                    <select name="brand" class="form-control js-select2-custom">
                                        @foreach($brands as $brand)
                                            <option @if(isset($product) && $product->brand==$brand->id) selected="selected" @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label class="input-label" for="exampleFormControlInput1">Trạng thái</label>
                                    <select name="status" class="form-control js-select2-custom">
                                        <option @if(isset($product) && $product->status==1) selected="selected" @endif value="1">Hoạt động</option>
                                        <option @if(isset($product) && $product->status==0) selected="selected" @endif value="0">Không hoạt động</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Hình ảnh sản phẩm</label><small
                                style="color: red">*</small>
                            <div>
                                @if(isset($images))
                                <div class="row mb-3">
                                    @foreach($images as $img)
                                        <div class="col-3">
                                            <img style="height: 200px;width: 100%"
                                                 src="{{asset('storage/admin/product/'.$product->name.'/'.$img)}}">
                                            <a href=""
                                               style="margin-top: -35px;border-radius: 0"
                                               class="btn btn-danger btn-block btn-sm">Remove</a>
                                        </div>
                                    @endforeach
                                </div>
                                @endif
                                <div class="row" id="coba"></div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <center>
                        <button type="submit" class="btn btn-primary">@if(isset($product))Cập nhật @else Thêm mới @endif</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script_2')
    <script src="{{asset('admin/assets/js/spartan-multi-image-picker.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $("#coba").spartanMultiImagePicker({
                fieldName: 'images[]',
                maxCount: 8,
                rowHeight: '215px',
                groupClassName: 'col-3',
                maxFileSize: '',
                placeholderImage: {
                    image: '{{asset('admin/assets/img/400x400/img2.jpg')}}',
                    width: '100%'
                },
                dropFileLabel: "Drop Here",
                onAddRow: function (index, file) {

                },
                onRenderedPreview: function (index) {

                },
                onRemoveRow: function (index) {

                },
                onExtensionErr: function (index, file) {
                    toastr.error('Đây không phải là ảnh', {
                        CloseButton: true,
                        ProgressBar: true
                    });
                },
                onSizeErr: function (index, file) {
                    toastr.options.timeOut =4000;
                    toastr.error('File size too big', {
                        CloseButton: true,
                        ProgressBar: true
                    });
                }
            });
        });
    </script>
    <script>
        function getRequest(route, id) {
            $.get({
                url: route,
                dataType: 'json',
                success: function (data) {
                    $('#' + id).empty().append(data.options);
                },
            });
        }
    </script>
@endpush
