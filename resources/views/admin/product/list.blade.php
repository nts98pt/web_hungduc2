@extends('layouts.admin.app')

@section('title', 'Danh sách sản phẩm')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-filter-list"></i> Danh sách Sản phẩm</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <!-- Card -->
                <div class="card">
                    <!-- Header -->
                    <div class="card-header" style="padding-right: 0;">
                        <div class="container row">
                            <div class="col-6">
                                <form action="javascript:" id="search-form">
                                    <!-- Search -->
                                    <div class="input-group input-group-merge input-group-flush">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tio-search"></i>
                                            </div>
                                        </div>
                                        <input id="datatableSearch_" type="search" name="search" class="form-control" placeholder="Tìm kiếm toàn cục" aria-label="Search" required>
                                        <button type="submit" class="btn btn-primary">Tìm kiếm</button>

                                    </div>
                                    <!-- End Search -->
                                </form>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('product.add') }}" class="btn btn-primary float-right"><i
                                        class="tio-add-circle"></i>
                                    Thêm mới Sản phẩm
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive datatable-custom">
                        <table id="columnSearchDatatable" class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table" data-hs-datatables-options='{
                                     "order": [],
                                     "orderCellsTop": true
                                   }'>
                            <thead class="thead-light">
                            <tr>
                                <th>Mã Sản phẩm</th>
                                <th style="width: 10%">Tên Sản phẩm</th>
                                <th style="width: 25%">Hình ảnh Sản phẩm</th>
                                <th>Trạng thái</th>
{{--                                <th>Daily needs</th>--}}
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody id="set-rows">
                            @foreach($products as $key=>$product)
                                <tr>
                                    <td>{{ $product['sku'] }}</td>
                                    <td>
                                    <span class="d-block font-size-sm text-body">
                                        <a href="{{ route('product.show',['id'=>$product['id']])}}">{{ $product['name'] }}</a>
                                    </span>
                                    </td>
                                    <td>
                                        <a href="{{ route('product.show',['id' => $product['id']]) }}">
                                            <div class="d-block font-size-sm text-body">
                                                <img src="{{ asset('storage/admin/product/'.$product->name.'/'.$product['image']['0']) }}" style="width: 100px" onerror="this.src='{{asset('admin/assets/img/160x160/img2.jpg')}}'">
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        @if($product['status']==1)
                                            <div style="padding: 10px;border: 1px solid;cursor: pointer"
                                                 onclick="location.href='{{route('product.status',['id'=> $product['id']])}}'">
                                                <span class="legend-indicator bg-success"></span>Hoạt động
                                            </div>
                                        @else
                                            <div style="padding: 10px;border: 1px solid;cursor: pointer"
                                                 onclick="location.href='{{ route('product.status',['id'=>$product['id']]) }}'">
                                                <span class="legend-indicator bg-danger"></span>Không hoạt động
                                            </div>
                                        @endif
                                    </td>
                                    {{--                                <td>--}}
                                    {{--                                    <label class="toggle-switch d-flex align-items-center mb-3" for="pending_status">--}}
                                    {{--                                        <input type="checkbox" name="pending_status" class="toggle-switch-input" onchange="daily_needs()" value="0" id="pending_status" >--}}
                                    {{--                                        <span class="toggle-switch-label">--}}
                                    {{--                                                    <span class="toggle-switch-indicator"></span>--}}
                                    {{--                                                </span>--}}
                                    {{--                                    </label>--}}
                                    {{--                                </td>--}}
                                    <td>{{ $product['selling_price'] }}</td>
                                    <td>
                                        <label class="badge badge-soft-info">{{ $product['total_stock'] }}</label>
                                    </td>
                                    <td>
                                        <!-- Dropdown -->
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="tio-settings"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ route('product.show', ['id' => $product['id']]) }}">Chi tiết</a>
                                                <a class="dropdown-item" href="{{ route('product.edit', ['id' => $product['id']]) }}">Cập nhật</a>
                                                <a class="dropdown-item" href="javascript:" onclick="form_alert('{{ $product['id'] }}','Bạn chắc chắn muốn xóa Sản phẩm')">Xóa</a>
                                                <form action="{{route('product.delete', ['id' => $product['id']])}}" method="POST" id="{{ $product['id'] }}">
                                                    @csrf @method('DELETE')
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Dropdown -->
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <hr>
                        <div class="page-area">
                            <table>
                                <tfoot class="border-top">
                                {!! $products->links() !!}
                                </tfoot>
                            </table>
                        </div>
                        </div>
                    </div>
                    <!-- End Table -->
                </div>
                <!-- End Card -->
            </div>
        </div>
    </div>
@endsection

@push('script_2')
    <script>
        $('#search-form').on('submit', function () {
            var formData = new FormData(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.post({
                url: '{{route('product.search')}}',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    $('#set-rows').html(data.view);
                    $('.page-area').hide();
                },
                complete: function () {
                    $('#loading').hide();
                },
            });
        });
    </script>
@endpush

