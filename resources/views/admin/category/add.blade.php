<form data-name="category" action="{{ isset($category) ? route('category.update', ['id' => $category->id]) : route('category.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-6">
            <div class="form-group  lang_form" id="en-form">
                <label class="input-label" for="exampleFormControlInput1">Tên danh mục</label>
                <input type="text" name="name" class="form-control" placeholder="Tên" value="{{isset($category) ? $category->name: ''}}" required>
            </div>
        </div>
        <div class="col-6 from_part_2">
            <label>Hình ảnh</label><small style="color: red">* ( Tỷ lệ 3:1 )</small>
            <div class="custom-file">
                <input type="file" name="image" id="customFileEg1" class="custom-file-input" accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*" {{isset($category) ? '' : 'required'}}>
                <label class="custom-file-label" for="customFileEg1">Chọn ảnh</label>
            </div>
        </div>
        <div class="col-12 from_part_2">
            <div class="form-group">
                <hr>
                <center>
                    <img style="width: 30%;border: 1px solid; border-radius: 10px;" id="viewer" src="{{isset($category) ? $category->image : asset('admin/assets/img/900x400/img1.jpg')}}" onerror="this.onerror=null; this.src='{{asset('admin/assets/img/900x400/img1.jpg')}}'" alt="image" />
                </center>
            </div>
        </div>
    </div>

    <hr>
    <button type="submit" class="btn btn-primary">{{isset($category) ? 'Cập nhật' : 'Thêm'}}</button>
</form>