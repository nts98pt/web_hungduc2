
<hr>
<div class="card">
    <div class="card-header">
        <h5 class="card-header-title"></h5>
    </div>
    <!-- Table -->
    <div class="table-responsive datatable-custom">
        <table id="columnSearchDatatable"
               class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table"
               data-hs-datatables-options='{
                             "order": [],
                             "pageLength": 99,
                             "language": {
                                    "emptyTable": "Không có dữ liệu",
                                    "zeroRecords": "Không có dữ liệu"
                              },
                             "orderCellsTop": true
                           }'>
            <thead class="thead-light">
            <tr>
                <th>#</th>
                <th style="width: 50%">Tên</th>
                <th style="width: 20%">Trạng thái</th>
                <th style="width: 10%">Hành động</th>
            </tr>
            <tr>
                <th></th>
                <th>
                    <input type="text" id="column1_search" class="form-control form-control-sm"
                           placeholder="Tìm kiếm">
                </th>
                <th>
                    <select id="column3_search" class="js-select2-custom" data-hs-select2-options='{
                                          "minimumResultsForSearch": "Infinity",
                                          "customClass": "custom-select custom-select-sm text-capitalize"  }'>
                        <option value="">Tất cả</option>
                        <option value="Hiện">Hiện</option>
                        <option value="Ẩn">Ẩn</option>
                    </select>
                </th>
                <th>

                </th>
            </tr>
            </thead>

            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>
                        <span class="d-block font-size-sm text-body">{{$category->name}}</span>
                    </td>
                    <td>
                        <div style="padding: 10px;border: 1px solid;cursor: pointer" data-action="change-status"
                             data-url="{{route('category.update', ['id' => $category->id])}}"
                             data-name="{{$category->name}}"
                             data-value="{{$category->status}}">
                            <span class="legend-indicator {{$category->status ? 'bg-success' : 'bg-danger'}}"></span>{{$category->status ? 'Hiện' : 'Ẩn'}}
                        </div>
                    </td>
                    <td>
                        <!-- Dropdown -->
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="tio-settings"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" data-action="btnEdit"
                                   href="{{route('category.edit', ['id' => $category->id])}}">Sửa</a>
                                <a class="dropdown-item" data-action="btnDelete" data-name="{{$category->name}}"
                                   href="{{route('category.destroy', ['id'=> $category->id])}}">Xóa</a>
                            </div>
                        </div>
                        <!-- End Dropdown -->
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <hr>
        <table>
            <tfoot>

            </tfoot>
        </table>
    </div>
</div>