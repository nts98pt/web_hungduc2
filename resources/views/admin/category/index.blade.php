@extends('layouts.admin.app')

@section('title', 'Danh mục sản phẩm')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->

        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-add-circle-outlined"></i> Thêm Mới Danh Mục Sản Phẩm</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        @if($errors->any())
            <p id="error-message" data-message="{{$errors->first()}}"></p>
        @endif
        @if(session('message') && session('type'))
            <p id="session-message" data-message="{{session('message')}}" data-type="{{session('type')}}"></p>
            @php(session()->forget(['message', 'type']))
        @endif
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2 category-form">
                {!! $inc_add !!}
            </div>
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2 category-list">
                {!! $inc_list !!}
            </div>
            <!-- End Table -->
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('admin/assets/js/common.js')}}"></script>
    <script src="{{asset('admin/assets/js/message.js')}}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#viewer').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '#customFileEg1', function (){
            readURL(this);
        })

    </script>
    <script>
        $(function () {
            $(document).on('click', '*[data-action="btnDelete"]', function () {
                event.preventDefault();
                var name = $(this).attr('data-name')
                var url = $(this).attr('href');
                var currentElement = $(this);

                app.deleteObject(`Danh mục sản phẩm ${name} sẽ bị xóa bỏ?`, url, currentElement)

            });
            $(document).on('click', '*[data-action="btnEdit"]', function () {
                event.preventDefault();
                var url = $(this).attr('href');
                var currentElement = $(this);
                let ajaxCall = $.ajax({
                    url     : url,
                    type    : 'get',
                    headers : {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                })

                ajaxCall.done(function (json) {
                    $('.category-form').html(json['data']['html'])
                });

            });
            $(document).on('submit', '*[data-name="category"]', function (e) {
                event.preventDefault();
                let currentElement = $(this)
                let url = currentElement.attr('action')
                var formData = new FormData(this);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.post({
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    success: function (json) {
                        if (json['success']) {
                            $('.category-list').html(json['data']['html']);
                            app.refreshDataTable();
                            toastr.options.timeOut = 3000;
                            if (json['data']['type'] === 'create') {
                                $('input[name="name"]').val('')
                                $('input[name="image"]').val('')
                                $('#viewer').attr('src','{{asset('admin/assets/img/900x400/img1.jpg')}}')
                                toastr.success(json['message']);
                            } else {
                                $('.category-form').html(json['data']['add_html'])
                                toastr.info(json['message']);
                            }
                        } else {
                            toastr.options.timeOut = 3000;
                            toastr.warning(json['message'])
                        }
                    },
                    error: function (json) {
                        if (json['status'] === 422) {
                            let error = json['responseJSON']['message']
                            toastr.options.timeOut = 3000;
                            toastr.error(error)
                        }
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                });
            })
            $(document).on('click', '*[data-action="change-status"]', function () {
                event.preventDefault();
                let url = $(this).attr('data-url');
                let currentElement = $(this);
                let name = currentElement.attr('data-name');
                let status = currentElement.attr('data-value');
                if (status == true) {
                    currentElement.attr('data-value', '0');
                } else {
                    currentElement.attr('data-value', '1');
                }
                let statusUpdate = status == true ? 0 : 1
                let ajaxCall = $.ajax({
                    url     : url,
                    type    : 'post',
                    data    : {
                        'status' : statusUpdate,
                        'name' : name
                    },
                    dataType: 'json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    headers : {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                })

                ajaxCall.done(function (json) {
                    if (json['success']) {
                        $('.category-list').html(json['data']['html']);
                        app.refreshDataTable();
                        toastr.options.timeOut = 3000;
                        toastr.info(json['message']);
                    } else {
                        toastr.options.timeOut = 3000;
                        toastr.warning(json['message'])
                    }
                });
            });
        });
    </script>
@endsection