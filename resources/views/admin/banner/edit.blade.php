@extends('layouts.admin.app')

@section('title', 'Cập nhật Banner')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-edit"></i> Cập nhật Banner</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="{{ route('banner.update',['id'=>$banner->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlInput1">Tiêu đề<small style="color: red">*</small></label>
                                <input value="{{ $banner->title }}" type="text" name="title" class="form-control" placeholder="Banner mới" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlInput1">URL</label>
                                <input value="{{ $banner->url }}" type="text" name="url" class="form-control" placeholder="URL">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" >Hình ảnh Banner<small style="color: red">*</small></label>
                                <div class="custom-file">
                                    <input type="file" name="image" id="customFileEg1" class="custom-file-input" accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*" >
                                    <label class="custom-file-label" for="customFileEg1">Chọn hình ảnh</label>
                                </div>
                                <hr>
                                <center>
                                    <img style="width: 80%;border: 1px solid; border-radius: 10px;" id="viewer" src="{{asset('storage/admin/banner/'.$banner->image)}}"
                                         onerror="daily_needs('','Hình ảnh lỗi');" alt="Hình ảnh Banner" />
                                </center>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group" id="type-product">
                                <label class="input-label" for="exampleFormControlSelect1">Trạng thái<small style="color: red">*</small>
                                </label>
                                <select name="status" class="form-control js-select2-custom">
                                    <option @if($banner->status == 1) selected="selected" @endif value="1">Hoạt động</option>
                                    <option @if($banner->status == 0) selected="selected" @endif value="0">Không hoạt động</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Cập nhật</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script_2')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewer').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#customFileEg1").change(function () {
            readURL(this);
        });


        function show_item(type) {
            if (type === 'product') {
                $("#type-product").show();
                $("#type-category").hide();
            } else {
                $("#type-product").hide();
                $("#type-category").show();
            }
        }
    </script>
@endpush
