@extends('layouts.admin.app')

@section('title', 'Danh sách banner')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-filter-list"></i> Danh sách Banner</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <!-- Card -->
                <div class="card">
                    <!-- Header -->
                    <div class="card-header">
                        <h5 class="card-header-title"></h5>
                        <a href="{{ route('banner.add') }}" class="btn btn-primary pull-right"><i class="tio-add-circle"></i>Thêm mới Banner</a>
                    </div>
                    <!-- End Header -->
                    <!-- Table -->
                    <div class="table-responsive datatable-custom">
                        <table id="columnSearchDatatable" class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table" data-hs-datatables-options='{
                                 "order": [],
                                 "orderCellsTop": true
                               }'>
                            <thead class="thead-light">
                            <tr>
                                <th>ID</th>
                                <th style="width: 25%">Tiêu đề</th>
                                <th style="width: 30%">Hình ảnh</th>
                                <th style="width: 25%">Url</th>
                                <th>Trạng thái</th>
                                <th style="width: 100px">Hành động</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    <input type="text" id="column1_search" class="form-control form-control-sm" placeholder="Tìm kiếm theo Tiêu đề Banner">
                                </th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($banners as $banner)
                            <tr>
                                <td>{{ $banner->id }}</td>
                                <td>
                                    <span class="d-block font-size-sm text-body">
                                        {{ $banner->title }}
                                    </span>
                                </td>
                                <td>
                                    <div class="d-block font-size-sm text-body">
                                        <img style="width: 100px;height: 100px" src="{{asset('storage/admin/banner/'.$banner->image)}}" >
                                    </div>
                                </td>
                                <td>
                                    <span class="d-block font-size-sm text-body">
                                        {{ $banner->url }}
                                    </span>
                                </td>
                                <td>
                                    @if($banner->status==1)
                                        <div style="padding: 10px;border: 1px solid;cursor: pointer"
                                             onclick="location.href='{{route('banner.status',['id'=> $banner->id])}}'">
                                            <span class="legend-indicator bg-success"></span>Hoạt động
                                        </div>
                                    @else
                                        <div style="padding: 10px;border: 1px solid;cursor: pointer"
                                             onclick="location.href='{{ route('banner.status',['id'=>$banner->id]) }}'">
                                            <span class="legend-indicator bg-danger"></span>Không hoạt động
                                        </div>
                                    @endif
                                </td>
                                <td>
                                    <!-- Dropdown -->
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="tio-settings"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ route('banner.edit', ['id' => $banner->id]) }}">Cập nhật</a>
                                            <a class="dropdown-item" href="javascript:" onclick="form_alert('{{ $banner->id }}','Bạn chắc chắn muốn xóa Banner')">Xóa</a>
                                            <form action="{{route('banner.delete', ['id' => $banner->id])}}" method="POST" id="{{ $banner->id }}">
                                                @csrf @method('DELETE')
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Dropdown -->
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <table>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <!-- End Table -->
                </div>
                <!-- End Card -->
            </div>
        </div>
    </div>
@endsection
