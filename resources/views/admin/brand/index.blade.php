@extends('layouts.admin.app')

@section('title', 'Thêm mới Brand')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-add-circle-outlined"></i> Thêm mới Nhãn hàng</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="{{ route('brand.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlInput1">Tên nhãn hàng<small style="color: red">*</small></label>
                                <input type="text" name="name" class="form-control" placeholder="Tên nhãn hàng" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlInput1">Kí hiệu<small style="color: red">*</small></label>
                                <input type="text" name="alias" class="form-control" placeholder="Kí hiệu hiển thị trên url" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" for="">Vị trí<small style="color: red">*</small></label>
                                <input type="number" name="sort" class="form-control" placeholder="Vị trí hiển thị" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group" id="type-product">
                                <label class="input-label" for="exampleFormControlSelect1">Trạng thái<small style="color: red">*</small>
                                </label>
                                <select name="status" class="form-control js-select2-custom">
                                    <option value="1">Hoạt động</option>
                                    <option value="0">Không hoạt động</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" >Hình ảnh Nhãn hàng<small style="color: red">*</small></label>
                                <div class="custom-file">
                                    <input type="file" name="image" id="customFileEg1" class="custom-file-input" accept=".jpg, .png,.svg, .jpeg, .gif, .bmp, .tif, .tiff|image/*" required>
                                    <label class="custom-file-label" for="customFileEg1">Chọn hình ảnh</label>
                                </div>
                                <hr>
                                <center >
                                    <img style="width: 80%;border: 1px solid; border-radius: 10px;" id="viewer" src="{{asset('admin/assets/img/900x400/img1.jpg') }}"
                                         onerror="daily_needs('','Hình ảnh lỗi');" alt="Hình ảnh Brand" />
                                </center>
                            </div>
                        </div>
                    </div>
                    <center>
                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                    </center>
                </form>
            </div>

            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <hr>
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-title"></h5>
                    </div>
                    <!-- Table -->
                    <div class="table-responsive datatable-custom">
                        <table id="columnSearchDatatable" class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table" data-hs-datatables-options='{
                                     "order": [],
                                     "orderCellsTop": true
                                   }'>
                            <thead class="thead-light">
                            <tr>
                                <th style="width: 5%">ID</th>
                                <th style="width: 15%">Tên Nhãn hàng</th>
                                <th style="width: 5%">Kí hiệu</th>
                                <th style="width: 10%">Hình ảnh Nhãn hàng</th>
                                <th style="width: 10%">Vị trí</th>
                                <th style="width: 10%">Trạng thái</th>
                                <th style="width: 10%">Hành động</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    <input type="text" id="column1_search" class="form-control form-control-sm" placeholder="Tìm kiếm theo tên Nhãn hàng">
                                </th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($brands as $brand)
                                <tr>
                                    <td>{{ $brand->id }}</td>
                                    <td>
                                        <span class="d-block font-size-sm text-body">{{ $brand->name }}
                                        </span>
                                    </td>
                                    <td>
                                        <span class="d-block font-size-sm text-body">{{ $brand->alias }}
                                        </span>
                                    </td>
                                    <td>
                                        <div class="d-block font-size-sm text-body">
                                            <img style="width: 100px; height: 100px;" src="{{asset('storage/admin/brand/'.$brand->image)}}"
                                                 onerror="this.src='{{asset('admin/assets/img/160x160/img2.jpg')}}'">
                                        </div>
                                    </td>
                                    <td>
                                        <span class="d-block font-size-sm text-body">{{ $brand->sort }}
                                        </span>
                                    </td>
                                    <td>
                                        @if($brand->status==1)
                                            <div style="padding: 10px;border: 1px solid;cursor: pointer"
                                                 onclick="location.href='{{route('brand.status',['id'=> $brand->id])}}'">
                                                <span class="legend-indicator bg-success"></span>Hoạt động
                                            </div>
                                        @else
                                            <div style="padding: 10px;border: 1px solid;cursor: pointer"
                                                 onclick="location.href='{{ route('brand.status',['id'=>$brand->id]) }}'">
                                                <span class="legend-indicator bg-danger"></span>Không hoạt động
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        <!-- Dropdown -->
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="tio-settings"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ route('brand.edit', ['id' => $brand->id]) }}">Cập nhật</a>
                                                <a class="dropdown-item" href="javascript:" onclick="form_alert('{{ $brand->id }}','Bạn chắc chắn muốn xóa Brand')">Xóa</a>
                                                <form action="{{route('brand.delete', ['id' => $brand->id])}}" method="POST" id="{{ $brand->id }}">
                                                    @csrf @method('DELETE')
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Dropdown -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <table>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End Table -->
        </div>
    </div>
@endsection
@push('script_2')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewer').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#customFileEg1").change(function () {
            readURL(this);
        });


        function show_item(type) {
            if (type === 'product') {
                $("#type-product").show();
                $("#type-category").hide();
            } else {
                $("#type-product").hide();
                $("#type-category").show();
            }
        }
    </script>
@endpush
