<form data-name="subcategory" action="{{ isset($subCategory) ? route('sub-category.update', ['id' => $subCategory->id]) : route('sub-category.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-6">
            <div class="form-group  lang_form" id="en-form">
                <label class="input-label" for="exampleFormControlInput1">Danh mục sản phẩm con</label>
                <input type="text" name="name" class="form-control" placeholder="Danh mục con"
                       value="{{isset($subCategory) ? $subCategory->name: ''}}"
                       required>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label class="input-label"
                       for="exampleFormControlSelect1">Danh mục cha
                    <span class="input-label-secondary">*</span></label>
                <select id="exampleFormControlSelect1" name="category_id" class="form-control" required>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" {{isset($subCategory) && $subCategory->category_id === $category->id ? 'selected' : ''}}>{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-6 from_part_2">
            <label>Hình ảnh</label><small style="color: red">* ( Tỷ lệ 3:1 )</small>
            <div class="custom-file">
                <input type="file" name="image" id="customFileEg1" class="custom-file-input"
                       accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*" {{isset($subCategory) ? '' : 'required'}}>
                <label class="custom-file-label" for="customFileEg1">Chọn ảnh</label>
            </div>
        </div>
        <div class="col-12 from_part_2">
            <div class="form-group">
                <hr>
                <center>
                    <img style="width: 30%;border: 1px solid; border-radius: 10px;" id="viewer"
                         src="{{isset($subCategory) ? $subCategory->image : asset('admin/assets/img/900x400/img1.jpg')}}"
                         onerror="this.onerror=null; this.src='{{asset('admin/assets/img/900x400/img1.jpg')}}'"
                         alt="image" />
                </center>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">{{isset($subCategory) ? 'Cập nhật' : 'Thêm'}}</button>
</form>