
@extends('layouts.admin.app')

@section('title', 'Danh mục thông báo')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-notifications"></i> Thông báo</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="{{ route('notification.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('POST')

                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">Tiêu đề</label>
                        <input type="text" name="title" class="form-control" placeholder="Thông báo mới" required>
                    </div>
                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">Nội dung</label>
                        <textarea name="description" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh</label><small style="color: red">* ( Tỷ lệ 3:1 )</small>
                        <div class="custom-file">
                            <input type="file" name="image" id="customFileEg1" class="custom-file-input" accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                            <label class="custom-file-label" for="customFileEg1">Chọn hình ảnh</label>
                        </div>
                        <hr>
                        <center>
                            <img style="width: 30%;border: 1px solid; border-radius: 10px;" id="viewer" src="{{asset('admin/assets/img/900x400/img1.jpg') }}" alt="image" />
                        </center>
                    </div>
                    <hr>

                    <button type="submit" class="btn btn-primary">Gửi Thông Báo</button>

                </form>
            </div>

            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <hr>
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-title"></h5>
                    </div>
                    <!-- Table -->
                    <div class="table-responsive datatable-custom">
                        <table id="columnSearchDatatable" class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table" data-hs-datatables-options='{
                                     "order": [],
                                      "info": {
                                         "currentInterval": "#datatableWithPaginationInfoCurrentInterval",
                                         "totalQty": "#datatableWithPaginationInfoTotalQty"
                                      },
                                     "pageLength": {{config('custom.limit')}},
                                     "isShowPaging": true,
                                     "pagination": "datatablePagination",
                                     "paginationClasses": "pagination mb-0",
                             "language": {
                                    "emptyTable": "Không có dữ liệu",
                                    "zeroRecords": "Không có dữ liệu"
                              },
                                     "orderCellsTop": true
                                   }'>
                            <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th style="width: 50%">Tiêu đề</th>
                                <th>Nội dung</th>
                                <th>Hình ảnh</th>
                                <th>Trạng thái</th>
                                <th style="width: 10%">Hành động</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    <input type="text" id="column1_search" class="form-control form-control-sm" placeholder="Tìm kiếm theo tên Thông báo">
                                </th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($notifications as $notification)
                            <tr>
                                <td> {{ $notification->id }}</td>
                                <td>
                                            <span class="d-block font-size-sm text-body">
                                           {{$notification->title}}
                                        </span>
                                </td>
                                <td>
                                    {{$notification->description}}
                                </td>
                                <td>
                                    <img style="height: 75px" src="{{asset('storage/admin/notification/'.$notification->image)}}"
                                         onerror="this.src='{{asset('admin/assets/img/160x160/img2.jpg')}}'">
                                </td>
                                <td>
                                    @if($notification->status==1)
                                    <div style="padding: 10px;border: 1px solid;cursor: pointer" onclick="location.href='{{route('notification.status',['id'=> $notification->id])}}'">
                                        <span class="legend-indicator bg-success"></span>Hoạt động
                                    </div>
                                    @else
                                        <div style="padding: 10px;border: 1px solid;cursor: pointer"
                                             onclick="location.href='{{ route('notification.status',['id'=>$notification->id]) }}'">
                                            <span class="legend-indicator bg-danger"></span>Không hoạt động
                                        </div>
                                    @endif
                                </td>
                                <td>
                                    <!-- Dropdown -->
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="tio-settings"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ route('notification.edit', ['id' => $notification->id]) }}">Cập nhật</a>
                                            <a class="dropdown-item" href="javascript:" onclick="form_alert('{{ $notification->id }}','Want to delete this item ?')">Xóa</a>
                                            <form action="{{route('notification.delete', ['id' => $notification->id])}}" method="post" id="{{ $notification->id }}">
                                            @csrf @method('DELETE')
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Dropdown -->
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <hr>
                        <table>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End Table -->
        </div>
    </div>
    <!-- Pagination -->
    <div class="d-flex justify-content-between align-items-center mt-2">
        <nav id="datatablePagination" class="m-2" aria-label="Activity pagination"></nav>
        <small class="text-body m-2">
            Hiển thị <span id="datatableWithPaginationInfoCurrentInterval"></span> tât cả <span id="datatableWithPaginationInfoTotalQty"></span> bản ghi
        </small>
    </div>
    <!-- End Pagination -->
@endsection
@push('script_2')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewer').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#customFileEg1").change(function () {
            readURL(this);
        });


        function show_item(type) {
            if (type === 'product') {
                $("#type-product").show();
                $("#type-category").hide();
            } else {
                $("#type-product").hide();
                $("#type-category").show();
            }
        }
    </script>
@endpush

@section('js')
    <script src="{{asset('admin/assets/js/common.js')}}"></script>
    <script src="{{asset('admin/assets/js/message.js')}}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#viewer').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '#customFileEg1', function (){
            readURL(this);
        })

    </script>
    <script>
        $(function () {
            $(document).on('click', '*[data-action="btnDelete"]', function () {
                event.preventDefault();
                var name = $(this).attr('data-name')
                var url = $(this).attr('href');
                var currentElement = $(this);

                app.deleteObject(`Danh mục thông báo ${name} sẽ bị xóa bỏ?`, url, currentElement, '.category-list')

            });
            $(document).on('click', '*[data-action="btnEdit"]', function () {
                event.preventDefault();
                var url = $(this).attr('href');
                var currentElement = $(this);
                let ajaxCall = $.ajax({
                    url     : url,
                    type    : 'get',
                    headers : {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                })

                ajaxCall.done(function (json) {
                    $('.category-form').html(json['data']['html'])
                });

            });
            $(document).on('submit', '*[data-name="category"]', function (e) {
                event.preventDefault();
                let currentElement = $(this)
                let url = currentElement.attr('action')
                var formData = new FormData(this);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.post({
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    success: function (json) {
                        if (json['success']) {
                            $('.category-list').html(json['data']['html']);
                            app.refreshDataTable();
                            toastr.options.timeOut = 3000;
                            if (json['data']['type'] === 'create') {
                                $('input[name="name"]').val('')
                                $('input[name="image"]').val('')
                                $('#viewer').attr('src','{{asset('admin/assets/img/900x400/img1.jpg')}}')
                                toastr.success(json['message']);
                            } else {
                                $('.category-form').html(json['data']['add_html'])
                                toastr.info(json['message']);
                            }
                        } else {
                            toastr.options.timeOut = 3000;
                            toastr.warning(json['message'])
                        }
                    },
                    error: function (json) {
                        if (json['status'] === 422) {
                            let error = json['responseJSON']['message']
                            toastr.options.timeOut = 3000;
                            toastr.error(error)
                        }
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                });
            })
            $(document).on('click', '*[data-action="change-status"]', function () {
                event.preventDefault();
                let url = $(this).attr('data-url');
                let currentElement = $(this);
                let name = currentElement.attr('data-name');
                let status = currentElement.attr('data-value');
                if (status == true) {
                    currentElement.attr('data-value', '0');
                } else {
                    currentElement.attr('data-value', '1');
                }
                let statusUpdate = status == true ? 0 : 1
                let ajaxCall = $.ajax({
                    url     : url,
                    type    : 'post',
                    data    : {
                        'status' : statusUpdate,
                        'name' : name
                    },
                    dataType: 'json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    headers : {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                })

                ajaxCall.done(function (json) {
                    if (json['success']) {
                        $('.category-list').html(json['data']['html']);
                        app.refreshDataTable();
                        toastr.options.timeOut = 3000;
                        toastr.info(json['message']);
                    } else {
                        toastr.options.timeOut = 3000;
                        toastr.warning(json['message'])
                    }
                });
            });
        });
    </script>
@endsection

