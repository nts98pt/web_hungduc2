<div id="sidebarMain" class="d-none">
    <aside
            class="js-navbar-vertical-aside navbar navbar-vertical-aside navbar-vertical navbar-vertical-fixed navbar-expand-xl navbar-bordered  ">
        <div class="navbar-vertical-container text-capitalize">
            <div class="navbar-vertical-footer-offset">
                <div class="navbar-brand-wrapper justify-content-between">
                    <!-- Logo -->
                    <a class="navbar-brand" href="" aria-label="Front">
                        <img class="navbar-brand-logo" onerror="this.src='assets/img/160x160/img2.jpg'"
                             src="{{asset('admin/assets/img/restaurant/2021-08-27-61288e2937af6.png')}}" alt="Logo">
                        <img class="navbar-brand-logo-mini" onerror="this.src='assets/img/160x160/img2.jpg'"
                             src="{{asset('admin/assets/img/restaurant/2021-08-27-61288e2937af6.png')}}" alt="Logo">
                    </a>
                    <!-- End Logo -->
                    <!-- Navbar Vertical Toggle -->
                    <button type="button" class="js-navbar-vertical-aside-toggle-invoker navbar-vertical-aside-toggle btn btn-icon btn-xs btn-ghost-dark">
                        <i class="tio-clear tio-lg"></i>
                    </button>
                    <!-- End Navbar Vertical Toggle -->
                </div>
                <!-- Content -->
                <div class="navbar-vertical-content">
                    <ul class="navbar-nav navbar-nav-lg nav-tabs">
                        <!-- Dashboards -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link"
                               href="" title="Dashboards">
                                <i class="tio-home-vs-1-outlined nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Dashboard
                                </span>
                            </a>
                        </li>
                        <!-- End Dashboards -->
                        <li class="nav-item">
                            <small class="nav-subtitle">Order Section</small>
                            <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                        </li>
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link nav-link-toggle" href="javascript:">
                                <i class="tio-shopping-cart nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Order
                                </span>
                            </a>
                            <ul class="js-navbar-vertical-aside-submenu nav nav-sub"
                                style="display: {{Request::is('')?'block':'none'}}">
                                <li class="nav-item ">
                                    <a class="nav-link" href="" title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            all
                                            <span class="badge badge-info badge-pill ml-1">
                                                3
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            Pending
                                            <span class="badge badge-soft-info badge-pill ml-1">
                                                2
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            Confirmed
                                                <span class="badge badge-soft-success badge-pill ml-1">
                                                0
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            Processing
                                                <span class="badge badge-warning badge-pill ml-1">
                                                0
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href=""
                                       title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            Out for delivery
                                                <span class="badge badge-warning badge-pill ml-1">
                                                0
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            Delivered
                                                <span class="badge badge-success badge-pill ml-1">
                                                1
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            Returned
                                                <span class="badge badge-soft-danger badge-pill ml-1">
                                                0
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            Failed
                                            <span class="badge badge-danger badge-pill ml-1">
                                                0
                                            </span>
                                        </span>
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">
                                            Canceled
                                                <span class="badge badge-soft-dark badge-pill ml-1">
                                                0
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End Pages -->
                        <li class="nav-item">
                            <small class="nav-subtitle">Product Section</small>
                            <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                        </li>
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu">
                            <a class="js-navbar-vertical-aside-menu-link nav-link nav-link-toggle" href="javascript:">
                                <i class="tio-image nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Banner</span>
                            </a>
                            <ul class="js-navbar-vertical-aside-submenu nav nav-sub"
                                style="display: {{Request::is('admin/banner*')?'block':'none'}}">
                                <li class="nav-item {{Request::is('admin/banner/add')?'active':''}}">
                                    <a class="nav-link " href="{{ route('banner.add') }}">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Add New</span>
                                    </a>
                                </li>
                                <li class="nav-item {{Request::is('admin/banner')?'active':''}}">
                                    <a class="nav-link " href="{{ route('banner') }}">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End Pages -->
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link nav-link-toggle" href="javascript:">
                                <i class="tio-category nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Category</span>
                            </a>
                            <ul class="js-navbar-vertical-aside-submenu nav nav-sub"
                                style="display: {{Request::is('')?'block':'none'}}">
                                <li class="nav-item ">
                                    <a class="nav-link " href="{{route('category.index')}}" title="add new category">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Category</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="{{route('sub-category.index')}}" title="add new sub category">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Sub Category</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End Pages -->
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-apps nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Attribute
                                </span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link nav-link-toggle" href="javascript:">
                                <i class="tio-premium-outlined nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Product</span>
                            </a>
                            <ul class="js-navbar-vertical-aside-submenu nav nav-sub"
                                style="display: {{Request::is('admin/product*')?'block':'none'}}">
                                <li class="nav-item {{Request::is('admin/product/add')?'active':''}}">
                                    <a class="nav-link " href="{{ route('product.add') }}" title="add new product">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Add New</span>
                                    </a>
                                </li>
                                <li class="nav-item {{Request::is('admin/product')?'active':''}}">
                                    <a class="nav-link " href="{{ route('product') }}" title="product list">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">List</span>
                                    </a>
                                </li>
                                <li class="nav-item {{Request::is('admin/product/import')?'active':''}}">
                                    <a class="nav-link " href="" title="import product">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Import Product</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End Pages -->
                        <li class="nav-item">
                            <small class="nav-subtitle" title="Layouts">Employee Handler</small>
                            <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                        </li>
                        <!-- Nav Item - Pages Collapse Menu -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href=""
                               title="Pages">
                                <i class="tio-incognito nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Role Management</span>
                            </a>
                        </li>
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link nav-link-toggle" href="javascript:"
                               title="Pages">
                                <i class="tio-user nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Employee</span>
                            </a>
                            <ul class="js-navbar-vertical-aside-submenu nav nav-sub"
                                style="display: {{Request::is('')?'block':'none'}}">
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="add new employee">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Add New</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title=" employee List">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <small class="nav-subtitle" title="Layouts">Business Section</small>
                            <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                        </li>
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu {{Request::is('admin/brand*')?'active':''}}">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="{{ route('brand') }}">
                                <i class="tio-shop nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Brand
                                </span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-messages nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Messages
                                </span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="" title="Pages">
                                <i class="tio-clock nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Set Time Slot </span>
                            </a>
                        </li>
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-star nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Product Reviews
                                </span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-notifications nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Send Notification
                                </span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-gift nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Coupon</span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link nav-link-toggle" href="javascript:">
                                <i class="tio-settings nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Settings</span>
                            </a>
                            <ul class="js-navbar-vertical-aside-submenu nav nav-sub"
                                style="display: {{Request::is('')?'block':'none'}}">
                                <li class="nav-item ">
                                    <a class="nav-link " href="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Shop Setup</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" title="add new  language">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Language</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Shipping Area</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="" >
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Location Setup</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Mail Config</span>
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link " href="" >
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">payment Methods</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href=""
                                       title="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Push Notification</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Terms &amp; Condition</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link "
                                       href="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Privacy Policy</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link "
                                       href="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">About Us</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End Pages -->
                        <li class="nav-item">
                            <small class="nav-subtitle" title="Layouts">Delivery Man Section</small>
                            <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                        </li>
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-running nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Register
                                </span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-filter-list nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    List
                                </span>
                            </a>
                        </li>
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-star-outlined nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Reviews
                                </span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <li class="nav-item">
                            <small class="nav-subtitle" title="Documentation">Customer Section</small>
                            <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                        </li>
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link" href="">
                                <i class="tio-poi-user nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">
                                    Customer List
                                </span>
                            </a>
                        </li>
                        <!-- End Pages -->
                        <li class="nav-item">
                            <div class="nav-divider"></div>
                        </li>
                        <li class="nav-item">
                            <small class="nav-subtitle" title="Documentation">Report &amp; Analytics</small>
                            <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                        </li>
                        <!-- Pages -->
                        <li class="navbar-vertical-aside-has-menu ">
                            <a class="js-navbar-vertical-aside-menu-link nav-link nav-link-toggle" href="javascript:">
                                <i class="tio-report-outlined nav-icon"></i>
                                <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate">Reports</span>
                            </a>
                            <ul class="js-navbar-vertical-aside-submenu nav nav-sub"
                                style="display: {{Request::is('')?'block':'none'}}">
                                <li class="nav-item ">
                                    <a class="nav-link " href="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Earning Report</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link " href="">
                                        <span class="tio-circle nav-indicator-icon"></span>
                                        <span class="text-truncate">Order Report</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End Pages -->
                        <li class="nav-item" style="padding-top: 100px">
                            <div class="nav-divider"></div>
                        </li>
                    </ul>
                </div>
                <!-- End Content -->
            </div>
        </div>
    </aside>
</div>
