<!DOCTYPE html>
<html lang="en">
<!-- head -->
@include('layouts.admin.partials.head')
<body class="footer-offset">
<!-- pending load -->
@include('layouts.admin.partials.loading')
<!-- header top -->
@include('layouts.admin.partials.header')
<!-- left sidebar-->
@include('layouts.admin.partials.sidebar')

<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main" class="main pointer-event">
    <!-- Content -->
    @hasSection('content')
        @yield('content')
    @endIf
    <!-- Footer -->
    @include('layouts.admin.partials.footer')
</main>
<!-- ========== END CONTENTS ========== -->

<script data-cfasync="false" src="{{asset('admin/assets/js/email-decode.min.js')}}"></script>
<script src="{{asset('admin/assets/js/custom.js')}}"></script>
<!-- JS Implementing Plugins -->
<!-- JS Front -->
<script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>
<script src="{{asset('admin/assets/js/theme.min.js')}}"></script>
<script src="{{asset('admin/assets/js/sweet_alert.js')}}"></script>
<script src="{{asset('admin/assets/js/toastr.js')}}"></script>
<script type="text/javascript"></script>

<script>
    $(document).on('ready', function() {
        if (window.localStorage.getItem('hs-builder-popover') === null) {
            $('#builderPopover').popover('show')
                .on('shown.bs.popover', function() {
                    $('.popover').last().addClass('popover-dark')
                });

            $(document).on('click', '#closeBuilderPopover', function() {
                window.localStorage.setItem('hs-builder-popover', true);
                $('#builderPopover').popover('dispose');
            });
        } else {
            $('#builderPopover').on('show.bs.popover', function() {
                return false
            });
        }
        var sidebar = $('.js-navbar-vertical-aside').hsSideNav();
        $('.js-nav-tooltip-link').tooltip({
            boundary: 'window'
        })

        $('.js-hs-unfold-invoker').each(function() {
            var unfold = new HSUnfold($(this)).init();
        });
    });
</script>
<script>
    $(".lang_link").click(function(e) {
        e.preventDefault();
        $(".lang_link").removeClass('active');
        $(".lang_form").addClass('d-none');
        $(this).addClass('active');
        let form_id = this.id;
        let lang = form_id.split("-")[0];
        console.log(lang);
        $("#" + lang + "-form").removeClass('d-none');
        if (lang == 'en') {
            $(".from_part_2").removeClass('d-none');
        } else {
            $(".from_part_2").addClass('d-none');
        }
    });
    $(document).on('ready', function() {
        var datatable = $.HSCore.components.HSDatatables.init($('#columnSearchDatatable'));
        $('#column1_search').on('keyup', function() {
            datatable
                .columns(1)
                .search(this.value)
                .draw();
        });
        $('#column3_search').on('change', function() {
            datatable
                .columns(2)
                .search(this.value)
                .draw();
        });
        $('.js-select2-custom').each(function() {
            var select2 = $.HSCore.components.HSSelect2.init($(this));
        });
    });

    if(false){
        $('#popup-modal').appendTo("body").modal('show');
    }

    function form_alert(id, message) {
        Swal.fire({
            title: 'Bạn chắc chắn ?',
            text: message,
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: 'default',
            confirmButtonColor: '#01684b',
            cancelButtonText: 'No',
            confirmButtonText: 'Yes',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $('#'+id).submit()
            }
        })
    }

    toastr.options.timeOut = 3000;
    function daily_needs(sussess='',error='',time=4000) {
        toastr.options.timeOut = time;
        if (sussess!=''){
            toastr.success(sussess);
        }
        if(error!='') {
            toastr.error(error);
        }
    }
</script>
@stack('script_2')
{!! Toastr::message() !!}
@hasSection('js')
    @yield('js')
@endif
</body>
</html>
