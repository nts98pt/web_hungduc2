<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\SubCategoryController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
    Route::get('index', [CategoryController::class, 'index'])
        ->name('index');
    Route::post('store', [CategoryController::class, 'store'])
        ->name('store');
    Route::get('edit/{id}', [CategoryController::class, 'edit'])
        ->name('edit');
    Route::post('update/{id}', [CategoryController::class, 'update'])
        ->name('update');
    Route::post('destroy/{id}', [CategoryController::class, 'destroy'])
        ->name('destroy');
});

Route::group(['prefix' => 'sub-category', 'as' => 'sub-category.'], function () {
    Route::get('index', [SubCategoryController::class, 'index'])
        ->name('index');
    Route::post('store', [SubCategoryController::class, 'store'])
        ->name('store');
    Route::get('edit/{id}', [SubCategoryController::class, 'edit'])
        ->name('edit');
    Route::post('update/{id}', [SubCategoryController::class, 'update'])
        ->name('update');
    Route::post('destroy/{id}', [SubCategoryController::class, 'destroy'])
        ->name('destroy');
});

Route::prefix('banner')->group(function () {
    Route::get('/', [BannerController::class, 'index'])->name('banner');
    Route::get('/add', function () {
        return view('admin.banner.index');
    })->name('banner.add');
    Route::post('/create', [BannerController::class, 'store'])->name('banner.store');
    Route::get('/edit/{id}', [BannerController::class, 'edit'])->name('banner.edit');
    Route::post('/update/{id}', [BannerController::class, 'update'])->name('banner.update');
    Route::get('/status/{id}', [BannerController::class, 'changeStatus'])->name('banner.status');
    Route::delete('/delete/{id}', [BannerController::class, 'delete'])->name('banner.delete');
});

Route::prefix('brand')->group(function () {
    Route::get('/', [BrandController::class, 'index'])->name('brand');
    Route::post('/store', [BrandController::class, 'store'])->name('brand.store');
    Route::get('/edit/{id}', [BrandController::class, 'edit'])->name('brand.edit');
    Route::post('/update/{id}', [BrandController::class, 'update'])->name('brand.update');
    Route::get('/status/{id}', [BrandController::class, 'changeStatus'])->name('brand.status');
    Route::delete('/delete/{id}', [BrandController::class, 'delete'])->name('brand.delete');
});

Route::prefix('product')->group(function () {
    Route::get('/', [ProductController::class, 'index'])->name('product');
    Route::get('/add', [ProductController::class, 'add'])->name('product.add');
    Route::post('/create', [ProductController::class, 'store'])->name('product.store');
    Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
    Route::post('/update/{id}', [ProductController::class, 'update'])->name('product.update');
    Route::delete('/delete/{id}', [ProductController::class, 'delete'])->name('product.delete');
    Route::get('/status/{id}', [ProductController::class, 'changeStatus'])->name('product.status');
    Route::post('/search', [ProductController::class, 'search'])->name('product.search');
    Route::get('/get-categories', [ProductController::class, 'get_categories'])->name('product.get-categories');
    Route::get('/show/{id}', [ProductController::class, 'show'])->name('product.show');
});

Route::prefix('notification')->group(function () {
    Route::get('/', [NotificationController::class, 'index'])->name('notification.index');
    Route::post('/store', [NotificationController::class, 'store'])->name('notification.store');
    Route::get('/edit/{id}', [NotificationController::class, 'edit'])->name('notification.edit');
    Route::post('/update/{id}', [NotificationController::class, 'update'])->name('notification.update');
    Route::get('/status/{id}', [NotificationController::class, 'changeStatus'])->name('notification.status');
    Route::delete('/delete/{id}', [NotificationController::class, 'delete'])->name('notification.delete');
});
