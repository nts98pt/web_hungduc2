<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'f_name' => 'Admin',
            'l_name' => 'Nguyễn Văn',
            'admin_role_id' => 1,
            'phone' => '0899354782',
            'email' => 'admim'.'@gmail.com',
            'image' => '',
            'password' => Hash::make('123456aA@'),
            'remember_token' => '',
        ]);
    }
}
