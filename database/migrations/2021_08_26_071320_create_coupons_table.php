<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100);
            $table->string('code', 50);
            $table->date('start_date');
            $table->date('expire_date');
            $table->decimal('min_purchase', 8, 2);
            $table->decimal('max_discount', 8, 2);
            $table->decimal('discount', 8, 2);
            $table->string('discount_type', 50);
            $table->integer('limit');
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
