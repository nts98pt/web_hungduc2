<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id()->startingValue(10000);
            $table->unsignedBigInteger('user_id');
            $table->string('full_name', 50);
            $table->text('address');
            $table->string('phone', 20);
            $table->decimal('total_order', 10, 2);
            $table->decimal('coupon_discount_amount', 10, 2);
            $table->unsignedBigInteger('coupon_id');
            $table->string('payment_status');
            $table->integer('order_status');
            $table->decimal('total_tax', 10, 2);
            $table->decimal('delivery_charge', 10, 2);
            $table->text('order_note');
            $table->date('delivery_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
