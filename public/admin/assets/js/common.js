var app = {
    deleteObject : function (text, url, currentElement) {
        Swal.fire({
            title: 'Bạn có chắc không?',
            text: text,
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: 'default',
            confirmButtonColor: '#01684b',
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Hủy bỏ',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url     : url,
                    type    : 'post',
                    dataType: 'json',
                    headers : {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (json) {
                        Swal.fire(
                            'Đã xóa!',
                            json['message'],
                            'success'
                        )
                        currentElement.closest('tr').css('background','tomato');
                        currentElement.closest('tr').fadeOut(800,function(){
                            $(this).remove();
                        });
                    },

                });

            }
        })
    },
    refreshDataTable: function () {
        let datatable = $.HSCore.components.HSDatatables.init($('#columnSearchDatatable'));
        $('.js-select2-custom').each(function () {
            var select2 = $.HSCore.components.HSSelect2.init($(this));
        });
        $('#column1_search').on('keyup', function () {
            datatable
                .columns(1)
                .search(this.value)
                .draw();
        });
        $('#column3_search').on('change', function () {
            datatable
                .columns(2)
                .search(this.value)
                .draw();
        });
        $('#column4_search').on('change', function () {
            datatable
                .columns(3)
                .search(this.value)
                .draw();
        });
    }

}
