$(function () {
    /*error notification*/
    let errorMessage = $("#error-message").attr('data-message');
    toastr.options.timeOut = 3000;
    if (errorMessage) {
        toastr.warning(errorMessage)
        $("#error-message").attr('data-message', '')
    }
    /*toast message*/
    let toastMessage = $("#session-message").attr('data-message');
    let type = $("#session-message").attr('data-type');
    if (toastMessage){
        if (type === 'success') {
            toastr.success(toastMessage);
        }
        if (type === 'info') {
            toastr.info(toastMessage);
        }
        $("#session-message").attr('data-message', '');
    }
});