<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CrtSubCategoryRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|unique:subcategories',
            'category_id' => 'required|numeric',
            'image'       => 'required|image|mimes:jpg,jpeg,png,gif,bmp,tif,tiff|max:5000'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'        => __('name_required', ['name' => 'danh mục con']),
            'name.unique'          => __('name_unique', ['name' => 'danh mục con']),
            'image.required'       => __('image_required', ['name' => 'danh mục con']),
            'category_id.required' => __('category_id_required'),
            'category_id.numeric'  => __('category_id_numeric'),
            'image.image'          => __('image_image'),
            'image.mimes'          => __('image_mimes'),
            'image.max'            => __('image_max', ['name' => '5MB']),
        ];
    }
}
