<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CrtCategoryRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|unique:categories',
            'image' => 'required|image|mimes:jpg,jpeg,png,gif,bmp,tif,tiff|max:5000'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'  => __('name_required', ['name' => 'danh mục']),
            'name.unique'    => __('name_unique', ['name' => 'danh mục']),
            'image.required' => __('image_required', ['name' => 'danh mục']),
            'image.image'    => __('image_image'),
            'image.mimes'    => __('image_mimes'),
            'image.max'      => __('image_max', ['name' => '5MB']),
        ];
    }
}
