<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


class APIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
    /**
     * If validator fails return the exception in json form
     *
     * @param Validator $validator
     *
     */
    protected function failedValidation(Validator $validator)
    {
        $json = [
            'status'  => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => $validator->errors()->first(),
            'errors'  => $validator->errors(),
        ];

        throw new HttpResponseException(response()->json($json,$json['status']));
    }
}
