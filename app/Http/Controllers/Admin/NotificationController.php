<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NotificationController extends Controller
{
    /*
    * Hiển thị thêm mới và danh sách Notification
    */
    public function index()
    {
        $notifications = Notification::all();
        return view('admin.notification.index', compact('notifications'));
    }

    /*
    * Thêm mới Notification
    */
    public function store(Request $request)
    {
        $image = [];
        if ($request->hasFile('image')) {
            $file = $request->file('image');

            if ($file->getSize() / (1024 * 1024) > 4) {
                Toastr::error('Hình ảnh phải nhỏ hơn hoặc bằng 4MB');
                return redirect()->back();
            }
            $extension = array('jpg', 'png', 'jpeg', 'svg', 'gif', 'bmp', 'tif', 'tiff');
            if (!in_array($file->clientExtension(), $extension)) {
                Toastr::error('Đây không phải là hình ảnh');
                return redirect()->back();
            }
            $image['name'] = time() . '_' . $request->file('image')->getClientOriginalName();
            $image['path'] = $request->file('image')->storeAs('public/admin/notification', $image['name']);
        }

        $notification = new Notification();
        $notification->title = $request->title;
        $notification->description = $request->description;
        $notification->image = $image ? $image['name'] : '';
        $notification->save();
        Toastr::success('Thêm mới Thông báo thành công');
        return redirect()->route('notification.index');
    }

    /*
   * Hiển thị cập nhật
   * @int $id
   */
    public function edit($id)
    {
        $notification = Notification::find($id);
        if (!is_numeric($id) || empty($notification)) {
            Toastr::error('Không tìm thấy bản ghi');
            return redirect()->back();
        }
        return view('admin.notification.edit', compact('notification'));
    }

    /*
    * Cập nhật Notification
    * @int id
    */
    public function update(Request $request, $id)
    {
        $find = Notification::find($id);
        if (!is_numeric($id) || empty($find)) {
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if (!empty($request->file('image'))) {
            $extension = array('jpg', 'png', 'jpeg', 'svg', 'gif', 'bmp', 'tif', 'tiff');
            if (!in_array($request->file('image')->clientExtension(), $extension)) {
                Toastr::error('Đây không phải là hình ảnh');
                return redirect()->back();
            }
            if ($request->file('image')->getSize() / (1024 * 1024) > 4) {
                Toastr::error('Hình ảnh phải nhỏ hơn hoặc bằng 4MB');
                return redirect()->back();
            }
            if (Storage::exists('public/admin/notification/' . $find->image)) {
                Storage::delete('public/admin/notification/' . $find->image);
            }
            $image_name = time() . '_' . $request->file('image')->getClientOriginalName();
            $path = $request->file('image')->storeAs('public/admin/notification', $image_name);
        } else {
            $image_name = $find->image;
        }
        Notification::where('id', $id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $image_name,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Toastr::success('Cập nhật Thông báo thành công');
        return redirect()->route('notification.index');
    }

    /*
     * Xóa Notification
     * @inrt $id
     */
    public function delete($id)
    {
        $find = Notification::find($id);
        if (!is_numeric($id) || empty($find)) {
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if (Storage::exists('public/admin/notification/' . $find->image)) {
            Storage::delete('public/admin/notification/' . $find->image);
        }
        Notification::where('id', $id)->delete();
        Toastr::success('Xóa Thông báo thành công');
        return redirect()->route('notification.index');
    }

    /*
     * Chuyển đổi trạng thái
     * @int $id
     */
    public function changeStatus($id)
    {
        $find = Notification::find($id);
        if (!is_numeric($id) || empty($find)) {
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if ($find->status == 1) {
            Notification::where('id', $id)->update([
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        } else {
            Notification::where('id', $id)->update([
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        Toastr::success('Chuyển đổi trạng thái thành công');
        return redirect()->route('notification.index');
    }
}
