<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /*
     * Danh sách sản phẩm
     */
    function index()
    {
        $products = Product::paginate(10);
        foreach ($products as $product){
            $product->image = json_decode($product->image);
        }
        return view('admin.product.list', compact('products'));
    }

    /*
     * Lấy danh sách sub-category theo category
     */
    public function get_categories(Request $request)
    {
        $subcategories = SubCategory::where([
            ['category_id',$request->id],
            ['status',1],
        ])->get();
        $res = '<option value="' . 0 . '" disabled selected>---Select---</option>';
        foreach ($subcategories as $row) {
            $res .= '<option value="' . $row->id . '" >' . $row->name . '</option>';
        }
        return response()->json([
            'options' => $res,
        ]);
    }

    /*
     * Hiển thị thêm mới
     */
    public function add(){
        $categories = Category::where('status',1)->select('id','name','image')->get();
        $brands = Brand::where('status',1)->select('id','name','alias','sort')->get();
        return view('admin.product.index',compact('categories','brands'));
    }

    /*
     * Thêm mới sản phẩm
     * Request $request
     */
    public function store(Request $request){
        $images = $request->file('images');
        foreach ($images as $image) {
            if($image->getSize()/(1024*1024)>4){
                Toastr::error('Hình ảnh phải nhỏ hơn hoặc bằng 4MB');
                return redirect()->back();
            }
            $extension = array('jpg','png','jpeg','svg','gif','bmp','tif','tiff');
            if (!in_array($image->clientExtension(),$extension)){
                Toastr::error('Đây không phải là hình ảnh');
                return redirect()->back();
            }
        }
        $count = 0;
        $folder = $request->name.'/';
        foreach ($images as $image){
            $images_name[$count++] = $image->getClientOriginalName();
            $image->storeAs('public/admin/product/'.$folder,$images_name[$count-1]);
        }
        $category = Category::find($request->category_id);
        $sub_category = SubCategory::find($request->sub_category_id);
        $product = new Product();
        $product->sku = $request->sku;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->category_id = json_encode(['category'=>['id' => $request->category_id, 'name'=>$category->name] , 'subcategory'=>['id' => $request->sub_category_id, 'name' => $sub_category->name]]);
        $product->brand_id = $request->brand_id;
        $product->selling_price = $request->selling_price;
        $product->total_stock = $request->total_stock;
        $product->discount = $request->get('discount',null);
        $product->discount_type = $request->get('discount_type',null);
        $product->tax = $request->get('tax',null);
        $product->tax_type = $request->get('tax_type',null);
        $product->image = json_encode($images_name);
        $product->status = $request->status;
        $product->save();
        Toastr::success('Thêm mới Sản phẩm thành công');
        return redirect()->route('product');
    }

    /*
     * Hiển thi cập nhật sản phẩm
     * @int $id
     */
    public function edit($id){
        $product = Product::find($id);
        if (!is_numeric($id) || empty($product)){
            Toastr::error('Không tìm thấy bản ghi');
            return redirect()->back();
        }
        $category_id = json_decode($product->category_id);
        $categories = Category::where('status',1)->select('id','name','image')->get();
        $brands = Brand::where('status',1)->select('id','name','alias','sort')->get();
        $sub_categories = SubCategory::where([
            ['category_id',$category_id->category->id],
            ['status',1],
        ])->get();
        $images = json_decode($product->image);
        return view('admin.product.index',compact('product','categories','brands','images','category_id','sub_categories'));
    }

    /*
     * Cập nhật sản phẩm
     * Request $request
     * @int $id
     */
    public function update(Request $request,$id){
        $find = Product::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if(!empty($request->file('images'))){
            $images = $request->file('images');
            foreach ($images as $image) {
                if($image->getSize()/(1024*1024)>4){
                    Toastr::error('Hình ảnh phải nhỏ hơn hoặc bằng 4MB');
                    return redirect()->back();
                }
                $extension = array('jpg','png','jpeg','svg','gif','bmp','tif','tiff');
                if (!in_array($image->clientExtension(),$extension)){
                    Toastr::error('Đây không phải là hình ảnh');
                    return redirect()->back();
                }
            }
            if (Storage::exists('public/admin/product/'.$find->name)) {
                Storage::deleteDirectory('public/admin/product/'.$find->name);
            }
            $count = 0;
            $folder = $request->name.'/';
            foreach ($images as $image){
                $images_name[$count++] = $image->getClientOriginalName();
                $image->storeAs('public/admin/product/'.$folder,$images_name[$count-1]);
            }
        } else{
            $image_name = json_decode($find->image);
        }
        $category = Category::find($request->category_id);
        $sub_category = SubCategory::find($request->sub_category_id);
        Product::where('id',$id)->update([
            'sku' => $request->sku,
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => json_encode(['category'=>['id' => $request->category_id, 'name'=>$category->name] , 'subcategory'=>['id' => $request->sub_category_id, 'name' => $sub_category->name]]),
            'brand_id' => $request->brand_id,
            'selling_price' => $request->selling_price,
            'total_stock' => $request->total_stock,
            'discount' => $request->get('discount',null),
            'discount_type' => $request->get('discount_type',null),
            'tax' => $request->get('tax',null),
            'tax_type' => $request->get('tax_type',null),
            'image' => json_encode($image_name),
            'status' => $request->status,
        ]);
        Toastr::success('Cập nhật Sản phẩm thành công');
        return redirect()->route('product');
    }

    /*
     * Xóa sản phẩm
     * @int $id
     */
    public function delete($id){
        $find = Product::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if (Storage::exists('public/admin/product/'.$find->name)) {
            Storage::deleteDirectory('public/admin/product/'.$find->name);
        }
        Product::where('id',$id)->delete();
        Toastr::success('Xóa Sản phẩm thành công');
        return redirect()->route('product');
    }

    /*
     * Chuyển đổi trạng thái
     * @int id
     */
    public function changeStatus($id){
        $find = Product::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if ($find->status==1){
            Product::where('id',$id)->update([
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        } else {
            Product::where('id',$id)->update([
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        Toastr::success('Chuyển đổi trạng thái thành công');
        return redirect()->route('product');
    }

    /*
     * Tìm kiếm Sản phẩm
     */
    public function search(Request $request)
    {
        $value = $request->search ;
        $products = Product::where('name', 'LIKE', "%{$value}%")->get();
        return response()->json([
            'view' => view('admin.product.inc.table', compact('products'))->render(),
        ]);
    }

    /*
     * Chi tiết Sản phẩm
     * @int $id
     */
    public function show($id){
        $product = Product::find($id);
        return view('admin.product.show',compact('product'));
    }
}
