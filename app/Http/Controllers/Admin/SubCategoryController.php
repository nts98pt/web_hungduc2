<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CrtSubCategoryRequest;
use App\Http\Requests\UptSubCategoryRequest;
use App\Models\Category;
use App\Models\SubCategory;
use App\Traits\StorageImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SubCategoryController extends Controller
{
    use  StorageImageTrait;

    private $subCategory;
    private $category;

    /**
     * SubCategoryController constructor.
     * @param SubCategory $subCategory
     * @param Category $category
     */
    public function __construct(SubCategory $subCategory, Category $category)
    {
        $this->subCategory = $subCategory;
        $this->category = $category;
    }


    public function index()
    {
        $categories = $this->category->get(['id', 'name']);
        $subCategories = $this->subCategory->get(['id', 'name', 'image', 'category_id', 'status']);
        $inc_add = view('admin.subcategory.add', compact('categories'));
        $inc_list = view('admin.subcategory.inc.list', compact('subCategories'))->render();
        return view('admin.subcategory.index', compact('inc_add', 'inc_list'));
    }

    public function store(CrtSubCategoryRequest $request)
    {
        try {
            $dataInsert = [
                'name'        => $request->input('name'),
                'category_id' => $request->input('category_id'),
            ];
            $image = $request->file('image');
            $imageSource = $this->storageTraitUpload($image, Str::slug($dataInsert['name']));
            $dataInsert['image'] = $imageSource['path'];

            $this->subCategory->create($dataInsert);
        } catch (\Exception $e) {
            $isCreated = false;
            Log::error('message: ' . $e->getMessage() . '--file : ' . $e->getFile() . '--Line : ' . $e->getLine());
        }

        if (isset($isCreated)) {
            $json = [
                'success' => false,
                'data'    => [
                    'type' => 'create',
                ],
                'message' => responseMessage('warning', 'create', __('sub_category')),
            ];
        } else {
            $subCategories = $this->subCategory->get(['id', 'name', 'image', 'category_id', 'status']);
            $inc_list = view('admin.subcategory.inc.list', compact('subCategories'))->render();
            $json = [
                'success' => true,
                'data'    => [
                    'type' => 'create',
                    'html' => $inc_list
                ],
                'message' => responseMessage('success', 'create', __('sub_category'))
            ];
        }

        return response()->json($json);
    }

    public function edit(Request $request, $id)
    {
        $subCategory = $this->subCategory->find($id);
        $categories = $this->category->get(['id', 'name']);
        $inc_add = view('admin.subcategory.add', compact('subCategory', 'categories'))->render();
        return response()->json([
            'data' => [
                'html' => $inc_add
            ]
        ]);
    }

    public function update(UptSubCategoryRequest $request, $id)
    {
        try {
            $subCategory = $this->subCategory->find($id);

            $dataUpdate = $request->only(['name', 'status', 'category_id']);
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageSource = $this->storageTraitUpload($image, Str::slug($dataUpdate['name']));
                $categoryUpdate['image'] = $imageSource['path'];
            }

            $isUpdate = $subCategory->update($dataUpdate);
        } catch (\Exception $e) {
            $isUpdate = false;
            Log::error('message: ' . $e->getMessage() . '--file : ' . $e->getFile() . '--Line : ' . $e->getLine());
        }

        if ($isUpdate) {
            $categories = $this->category->get(['id', 'name']);
            $subCategories = $this->subCategory->get(['id', 'name', 'image', 'category_id', 'status']);
            $inc_add = view('admin.subcategory.add', compact('categories'))->render();
            $inc_list = view('admin.subcategory.inc.list', compact('subCategories'))->render();
            $json = [
                'success' => true,
                'data' => [
                    'type' => 'update',
                    'html' => $inc_list,
                    'add_html' => $inc_add
                ],
                'message' =>  responseMessage('success', 'update', __('sub_category'))
            ];

            if ($request->has('status')) {
                $json['data']['status'] = $request->status;
                $json['message'] = __('change_status_success');
            }
        } else {
            $json = [
                'success' => false,
                'data' => [
                    'type' => 'update',
                ],
                'message' =>  responseMessage('warning', 'update', __('sub_category')),
            ];

            if ($request->has('status')) {
                $json['data']['status'] = $request->status;
                $json['message'] = __('change_status_false');
            }
        }

        return response()->json($json);
    }

    public function destroy(Request $request, $id)
    {
        try {
            $subCategory = $this->subCategory->find($id);
            $subCategory->delete();
        } catch (\Exception $e) {
            $isDeleted = false;
            Log::error('message: ' . $e->getMessage() . '--file : ' . $e->getFile() . '--Line : ' . $e->getLine());
        }

        return response()->json([
            'success' => true,
            'message' => responseMessage('success', 'delete', __('category'))
        ]);
    }
}
