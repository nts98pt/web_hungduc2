<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BrandController extends Controller
{
    /*
     * Hiển thị thêm mới và danh sách Brand
     */
    public function index(){
        $brands = Brand::all();
        return view('admin.brand.index',compact('brands'));
    }

    /*
     * Thêm mới Brand
     */
    public function store(Request $request){
        if(!is_numeric($request->sort)){
            Toastr::error('Vị trí phải là số');
            return redirect()->back();
        }
        if($request->file('image')->getSize()/(1024*1024)>4){
            Toastr::error('Hình ảnh phải nhỏ hơn hoặc bằng 4MB');
            return redirect()->back();
        }
        $extension = array('jpg','png','jpeg','svg','gif','bmp','tif','tiff');
        if (!in_array($request->file('image')->clientExtension(),$extension)){
            Toastr::error('Đây không phải là hình ảnh');
            return redirect()->back();
        }
        $image_name = time().'_'.$request->file('image')->getClientOriginalName();
        $path = $request->file('image')->storeAs('public/admin/brand',$image_name);
        $brand = new Brand();
        $brand->name = $request->name;
        $brand->alias = $request->alias;
        $brand->sort = $request->sort;
        $brand->image = $image_name;
        $brand->status = $request->status;
        $brand->save();
        Toastr::success('Thêm mới Brand thành công');
        return redirect()->route('brand');
    }

    /*
     * Hiển thị cập nhật
     * @int $id
     */
    public function edit($id){
        $brand = Brand::find($id);
        if (!is_numeric($id) || empty($brand)){
            Toastr::error('Không tìm thấy bản ghi');
            return redirect()->back();
        }
        return view('admin.brand.edit',compact('brand'));
    }

    /*
     * Cập nhật Brand
     * @int id
     */
    public function update(Request $request,$id){
        $find = Brand::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if(!is_numeric($request->sort)){
            Toastr::error('Vị trí phải là số');
            return redirect()->back();
        }
        if(!empty($request->file('image'))){
            $extension = array('jpg','png','jpeg','svg','gif','bmp','tif','tiff');
            if (!in_array($request->file('image')->clientExtension(),$extension)){
                Toastr::error('Đây không phải là hình ảnh');
                return redirect()->back();
            }
            if($request->file('image')->getSize()/(1024*1024)>4){
                Toastr::error('Hình ảnh phải nhỏ hơn hoặc bằng 4MB');
                return redirect()->back();
            }
            if (Storage::exists('public/admin/brand/'.$find->image)) {
                Storage::delete('public/admin/brand/'.$find->image);
            }
            $image_name = time().'_'.$request->file('image')->getClientOriginalName();
            $path = $request->file('image')->storeAs('public/admin/brand',$image_name);
        } else{
            $image_name = $find->image;
        }
        Brand::where('id',$id)->update([
            'name' => $request->name,
            'alias' => $request->alias,
            'sort' => $request->sort,
            'image' => $image_name,
            'status' => $request->status,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Toastr::success('Cập nhật Brand thành công');
        return redirect()->route('brand');
    }

    /*
     * Xóa Brand
     * @inrt $id
     */
    public function delete($id){
        $find = Brand::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if (Storage::exists('public/admin/brand/'.$find->image)) {
            Storage::delete('public/admin/brand/'.$find->image);
        }
        Brand::where('id',$id)->delete();
        Toastr::success('Xóa Banner thành công');
        return redirect()->route('brand');
    }

    /*
     * Chuyển đổi trạng thái
     * @int $id
     */
    public function changeStatus($id){
        $find = Brand::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if ($find->status==1){
            Brand::where('id',$id)->update([
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        } else {
            Brand::where('id',$id)->update([
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        Toastr::success('Chuyển đổi trạng thái thành công');
        return redirect()->route('brand');
    }
}
