<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Storage;


class BannerController extends Controller
{
    /*
     * Danh sach Banner
     */
    public function index(){
        $banners =  Banner::all();
        return view('admin.banner.list',compact('banners'));
    }

    /*
     * Thêm mới Banner
     * Request $request
     */
    public function store(Request $request){
        if($request->file('image')->getSize()/(1024*1024)>4){
            Toastr::error('Hình ảnh phải nhỏ hơn hoặc bằng 4MB');
            return redirect()->back();
        }
        $extension = array('jpg','png','jpeg','svg','gif','bmp','tif','tiff');
        if (!in_array($request->file('image')->clientExtension(),$extension)){
            Toastr::error('Đây không phải là hình ảnh');
            return redirect()->back();
        }
        $image_name = time().'_'.$request->file('image')->getClientOriginalName();
        $path = $request->file('image')->storeAs('public/admin/banner',$image_name);
        $banner = new Banner();
        $banner->title = $request->title;
        $banner->url = $request->get('url',null);
        $banner->image = $image_name;
        $banner->status = $request->status;
        $banner->save();
        Toastr::success('Thêm mới Banner thành công');
        return redirect()->route('banner');
    }

    /*
     * Hiển thi cập nhật Banner
     * @int $id
     */
    public function edit($id){
        $banner = Banner::find($id);
        if (!is_numeric($id) || empty($banner)){
            Toastr::error('Không tìm thấy bản ghi');
            return redirect()->back();
        }
        return view('admin.banner.edit',compact('banner'));
    }

    /*
     * Cập nhật Banner
     * Request $request
     * @int $id
     */
    public function update(Request $request,$id){
        $find = Banner::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if(!empty($request->file('image'))){
            $extension = array('jpg','png','jpeg','svg','gif','bmp','tif','tiff');
            if (!in_array($request->file('image')->clientExtension(),$extension)){
                Toastr::error('Đây không phải là hình ảnh');
                return redirect()->back();
            }
            if($request->file('image')->getSize()/(1024*1024)>4){
                Toastr::error('Hình ảnh phải nhỏ hơn hoặc bằng 4MB');
                return redirect()->back();
            }
            if (Storage::exists('public/admin/banner/'.$find->image)) {
                Storage::delete('public/admin/banner/'.$find->image);
            }
            $image_name = time().'_'.$request->file('image')->getClientOriginalName();
            $path = $request->file('image')->storeAs('public/admin/banner',$image_name);
        } else{
            $image_name = $find->image;
        }
        Banner::where('id',$id)->update([
            'title' => $request->title,
            'image' => $image_name,
            'url' => $request->get('url',null),
            'status' => $request->status,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Toastr::success('Cập nhật Banner thành công');
        return redirect()->route('banner');
    }

    /*
     * Xóa Banner
     * @int $id
     */
    public function delete($id){
        $find = Banner::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if (Storage::exists('public/admin/banner/'.$find->image)) {
            Storage::delete('public/admin/banner/'.$find->image);
        }
        Banner::where('id',$id)->delete();
        Toastr::success('Xóa Banner thành công');
        return redirect()->route('banner');
    }

    /*
     * Chuyển đổi trạng thái
     * @int id
     */
    public function changeStatus($id){
        $find = Banner::find($id);
        if (!is_numeric($id) || empty($find)){
            Toastr::error('Bản ghi không tồn tại');
            return redirect()->back();
        }
        if ($find->status==1){
            Banner::where('id',$id)->update([
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        } else {
            Banner::where('id',$id)->update([
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        Toastr::success('Chuyển đổi trạng thái thành công');
        return redirect()->route('banner');
    }
}
