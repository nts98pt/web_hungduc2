<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CrtCategoryRequest;
use App\Http\Requests\UptCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Traits\StorageImageTrait;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    use  StorageImageTrait;
    private $category;

    /**
     * CategoryController constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }


    public function index()
    {
        $categories = $this->category->all();
        $inc_add = view('admin.category.add')->render();
        $inc_list = view('admin.category.inc.list', compact('categories'))->render();
        return view('admin.category.index', compact('inc_list', 'inc_add'));
    }

    /**
     * create new category product
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $name = $request->input('name');
            $image = $request->file('image');
            $imageSource = $this->storageTraitUpload($image, Str::slug($name));
            $categoryInsert = [
                'name' => $name,
                'image'=> $imageSource['path']
            ];
            $this->category->create($categoryInsert);
        } catch (\Exception $e) {
            $isCreated = false;
            Log::error('message: ' . $e->getMessage() . '--file : ' . $e->getFile() . '--Line : ' . $e->getLine());
        }

        if (isset($isCreated)) {
            $json = [
                'success' => false,
                'data' => [
                    'type' => 'create',
                ],
                'message' =>  responseMessage('warning', 'create', __('category')),
            ];
        }else{
            $categories = $this->category->all();
            $inc_list = view('admin.category.inc.list', compact('categories'))->render();
            $json = [
                'success' => true,
                'data' => [
                    'type' => 'create',
                    'html' => $inc_list
                ],
                'message' =>  responseMessage('success', 'create', __('category'))
            ];
        }

        return response()->json($json);
    }

    public function edit(Request $request, $id)
    {
        $category = $this->category->find($id);
        $inc_add = view('admin.category.add', compact('category'))->render();
        return response()->json([
            'data' => [
                'html' => $inc_add
            ]
        ]);
    }

    public function update(UptCategoryRequest $request, $id)
    {
        try {
            $category = $this->category->find($id);

            $categoryUpdate = $request->only(['name', 'status']);
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageSource = $this->storageTraitUpload($image, Str::slug($categoryUpdate['name']));
                $categoryUpdate['image'] = $imageSource['path'];
            }

            $isUpdate = $category->update($categoryUpdate);
        } catch (\Exception $e) {
            $isUpdate = false;
            Log::error('message: ' . $e->getMessage() . '--file : ' . $e->getFile() . '--Line : ' . $e->getLine());
        }

        if ($isUpdate) {
            $categories = $this->category->all();
            $inc_add = view('admin.category.add')->render();
            $inc_list = view('admin.category.inc.list', compact('categories'))->render();
            $json = [
                'success' => true,
                'data' => [
                    'type' => 'update',
                    'html' => $inc_list,
                    'add_html' => $inc_add
                ],
                'message' =>  responseMessage('success', 'update', __('category'))
            ];

            if ($request->has('status')) {
                $json['data']['status'] = $request->status;
                $json['message'] = __('change_status_success');
            }
        }else{
            $json = [
                'success' => false,
                'data' => [
                    'type' => 'update',
                ],
                'message' =>  responseMessage('warning', 'update', __('category')),
            ];

            if ($request->has('status')) {
                $json['data']['status'] = $request->status;
                $json['message'] = __('change_status_false');
            }
        }

        return response()->json($json);

    }

    public function destroy(Request $request, $id)
    {
        try {
            $category = $this->category->find($id);
            $category->delete();
        } catch (\Exception $e) {
            $isDeleted = false;
            Log::error('message: ' . $e->getMessage() . '--file : ' . $e->getFile() . '--Line : ' . $e->getLine());
        }

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => responseMessage('success', 'delete', __('category'))
            ]);
        }

        if (isset($isDeleted)) {
            return redirect()->back()->withErrors([
                'error' => responseMessage('warning', 'delete', __('category')),
            ]);
        }

        return redirect()->route('category.index')
            ->with('message', responseMessage('success', 'create', __('category')))
            ->with('type', __('type_success'));
    }
}
