<?php
/**
 * @param string $type
 * @param string $action
 * @param string $name
 * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
 */
function responseMessage(string $type, string $action = '', string $name= ''){
    switch ($type){
        case __('type_success'):
            if ($action == 'create'){
                return __('success_create_message', ['name'=>$name]);
            }

            if ($action == 'update'){
                return __('success_update_message', ['name'=>$name]);
            }

            if ($action == 'delete'){
                return __('success_delete_message', ['name'=>$name]);
            }
            break;
        case __('type_warning'):
            if($action == 'create'){
                return __('warning_create_message', ['name'=>$name]);
            }

            if($action == 'update'){
                return __('warning_update_message', ['name'=>$name]);
            }

            if($action == 'delete'){
                return __('warning_delete_message', ['name'=>$name]);
            }
            break;
        default:
            return __('error_message');
    }
}

