<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SubCategory
 * @package App\Models
 * @property int $id
 * @property string $image
 * @property int $category_id
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */
class SubCategory extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'subcategories';
    protected $attributes = [
        'status' => true
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
