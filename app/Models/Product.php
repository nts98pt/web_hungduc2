<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $table='products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'sku',
        'name',
        'description',
        'image',
        'selling_price',
        'total_stock',
        'category_id',
        'brand_id',
        'discount',
        'discount_type',
        'tax',
        'tax_type',
        'status',
    ];
}
