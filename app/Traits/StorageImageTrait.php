<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


trait StorageImageTrait
{
    /**
     * @param $file
     * @param string $imageName
     * @param string $folderName
     * @return array
     */
    public function storageTraitUpload($file, string $imageName, string $folderName = 'image')
    {
        $ext = $file->getClientOriginalExtension();
        $fileName = Str::slug($imageName) . '.' . $ext;
        $filePath = $file->storeAs('public' . '/' . $folderName, $fileName);
        return [
            'name' => $fileName,
            'path' => Storage::url($filePath)
        ];
    }


}